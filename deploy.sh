#!/bin/bash

# Set default tag if not provided
if [ -z "$1" ]; then
  echo "No tag provided. Exiting..."
  exit 1
fi

NEW_TAG="v-05"  # This is the new tag you want to deploy with
CURRENT_TAG="v-04"  # This is your current tag

echo "Deploying with new tag: $NEW_TAG and maintaining current tag: $CURRENT_TAG"

# Replace placeholders in the YAML file for current and new deployment tags
sed -i "s|image: us-docker.pkg.dev/april-pro/bootstrap/bootstrap:\$TAG_PLACEHOLDER|image: us-docker.pkg.dev/april-pro/bootstrap/bootstrap:$NEW_TAG|" bootstrap-test.yml
sed -i "s|tag: REPLACE_TAG|tag: $CURRENT_TAG|" bootstrap-test.yml  # Set current tag
sed -i "/latestRevision: true/a \ \ - tag: $NEW_TAG\n\ \ \ percent: 0" bootstrap-test.yml  # Append new tag

# Deploy using the updated YAML
gcloud run services replace bootstrap-test.yml --platform managed --region us-east1 --project april-pro
