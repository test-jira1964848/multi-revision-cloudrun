# Use the latest Nginx image from Docker Hub
FROM nginx:latest

# Copy the frontend files to the Nginx document root
COPY index.html /usr/share/nginx/html/
COPY css/ /usr/share/nginx/html/css/
COPY js/ /usr/share/nginx/html/js/
COPY assets/ /usr/share/nginx/html/assets/

# Expose port 80 for external access (the default for Nginx)
EXPOSE 80

# Use the default CMD for Nginx
CMD ["nginx", "-g", "daemon off;"]